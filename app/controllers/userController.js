const userModel = require('../models/userModel');
const mongoose = require('mongoose');

const UserController = {
    //create new user 
    createNewUser: (req,res) => {
        //B1 : thu thập dữ liệu 
        let bodyReq = req.body;
        let newUser = {
            _id: mongoose.Types.ObjectId(),
            fullName: bodyReq.fullName,
            email: bodyReq.email,
            address: bodyReq.address,
            phone: bodyReq.phone,
        }
        userModel.create(newUser, (err,data) => {
            if (err) {
                return res.status(500).json({
                    status: 'Error 500: Internal server error',
                    message: err.message
                })            
            } else {
                return res.status(201).json({
                    status: 'Created User successfully!',
                    data: data
                })
            }
        })
    },

    //get all users
    getAllUsers: (req,res) => {
        userModel.find((err,data) => {
            res.status(200).json({
                status: 'GET all users',
                UsersList: data
            })
        }).populate('orders');
    },

    //get user by Id
    getUserById: (req,res) => {
        //B1: thu thập dữ liệu
        let userId = req.params.userId;
        //B2: kiểm tra dữ liệu
        if (!mongoose.Types.ObjectId.isValid(userId)) {
            return res.status(400).json({
                status: 'Error 400: Bad request',
                message: 'User Id is not valid!'
            })
        }
        else {
            userModel.findById(userId, (err,data) => {
                if (err) {
                    return res.status(500).json({
                        status: 'Error 500: Internal server error',
                        message: err.message
                    })
                } else {
                    return res.status(200).json({
                        status: 'User Found!',
                        User: data
                    })
                }
            })
        }
    },

    //update user by Id
    updateUserById: (req,res) => {
        //B1: thu thập dữ liệu
        let userId = req.params.userId;
        let bodyReq = req.body;
        //B2: kiểm tra dữ liệu
        if (!mongoose.Types.ObjectId.isValid(userId)) {
            return res.status(400).json({
                status: 'Error 400: Bad request',
                message: 'User Id is not valid!'
            })
        };
        //B3: thao tác với CSDL
        let userUpdated = {
            fullName: bodyReq.fullName,
            email: bodyReq.email,
            address: bodyReq.address,
            phone: bodyReq.phone,
        }
        userModel.findByIdAndUpdate(userId, userUpdated, (err,data) => {
            if (err) {
                return res.status(500).json({
                    status: 'Error 500: Internal server error',
                    message: err.message
                })
            }
            else {
                return res.status(200).json({
                    status: 'User Updated Successfully!',
                    userUpdated
                })
            }
        })
    },

    //delete user by Id
    deleteUserById: (req,res) => {
         //B1: thu thập dữ liệu
         let userId = req.params.userId;
         //B2: kiểm tra dữ liệu
         if (!mongoose.Types.ObjectId.isValid(userId)) {
             return res.status(400).json({
                 status: 'Error 400: Bad request',
                 message: 'User Id is not valid!'
             })
         };
        
         userModel.findByIdAndDelete(userId, (err,data) => {
             if (err) {
                 return res.status(500).json({
                     status: 'Error 500: Internal server error',
                     message: err.message
                 })
             }
             else {
                 return res.status(204).json({
                     status: 'User Deleted Successfully!',
                 })
             }
         })
     },
};

//export
module.exports = UserController;
