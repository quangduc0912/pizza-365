//import thư viện express
const voucherRouter = require('express').Router();

//import Controller
const { getAllVouchers, createVoucher, getVoucherById, updateVoucherById, deleteVoucherById } = require('../controllers/voucherController');

//get all vouchers
voucherRouter.get('/vouchers', getAllVouchers);

//create a new voucher
voucherRouter.post('/vouchers', createVoucher);

//get a voucher
voucherRouter.get('/vouchers/:voucherId', getVoucherById);

//update a voucher 
voucherRouter.put('/vouchers/:voucherId', updateVoucherById);

// //delete a voucher
voucherRouter.delete('/vouchers/:voucherId', deleteVoucherById);


//export 
module.exports = voucherRouter