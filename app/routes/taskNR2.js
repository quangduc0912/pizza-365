const taskNR2_Router = require('express').Router();

const userModel = require('../models/userModel');

taskNR2_Router.get('/limit-users', (req,res) => {
    let limitNumber = req.query.limit;
    if (limitNumber) {
        userModel.find().limit(limitNumber).exec( (err,data) => {
            if (err) {
                return res.status(500).json({
                    message: `Internal server error: ${err.message}`
                })
            } else {
                return res.status(200).json({
                    status: 'GET limit users',
                    UsersList: data
                })
            }
        })
        } else {
        userModel.find( (err,data) => {
            res.status(200).json({
                status: 'GET all users',
                UsersList: data
            })
        })
    }
});

taskNR2_Router.get('/skip-users', (req,res) => {
    let skipNumber = req.query.skip;
    if (skipNumber) {
        userModel.find().skip(skipNumber).exec( (err,data) => {
            if (err) {
                return res.status(500).json({
                    message: `Internal server error: ${err.message}`
                })
            } else {
                return res.status(200).json({
                    status: 'GET skip users',
                    UsersList: data
                })
            }
        })
        } else {
        userModel.find( (err,data) => {
            res.status(200).json({
                status: 'GET all users',
                UsersList: data
            })
        })
    }
});

taskNR2_Router.get('/sort-users', (req,res) => {
    userModel.find().sort({ fullName: 1 }).exec( (err,data) => {
        if (err) {
            return res.status(500).json({
                message: `Internal server error: ${err.message}`
            })
        } else {
            return res.status(200).json({
                status: 'GET sort users',
                UsersList: data
            })
        }
    })

});

taskNR2_Router.get('/skip-limit-users', (req,res) => {
    let limitNumber = req.query.limit;
    let skipNumber = req.query.skip;
    if (limitNumber && skipNumber) {
        userModel.find().skip(skipNumber).limit(limitNumber).exec( (err,data) => {
            if (err) {
                return res.status(500).json({
                    message: `Internal server error: ${err.message}`
                })
            } else {
                return res.status(200).json({
                    status: 'GET limit of skip users',
                    UsersList: data
                })
            }
        })
        } else {
        userModel.find( (err,data) => {
            res.status(200).json({
                status: 'GET all users',
                UsersList: data
            })
        })
    }
});

taskNR2_Router.get('/sort-skip-limit-users', (req,res) => {
    let limitNumber = req.query.limit;
    let skipNumber = req.query.skip;
    if (limitNumber && skipNumber) {
        userModel.find().sort({ fullName: 1 }).skip(skipNumber).limit(limitNumber).exec( (err,data) => {
            if (err) {
                return res.status(500).json({
                    message: `Internal server error: ${err.message}`
                })
            } else {
                return res.status(200).json({
                    status: `GET limit of skip sort users's fullName`,
                    UsersList: data
                })
            }
        })
        } else {
        userModel.find( (err,data) => {
            res.status(200).json({
                status: 'GET all users',
                UsersList: data
            })
        })
    }
});

module.exports = taskNR2_Router;
