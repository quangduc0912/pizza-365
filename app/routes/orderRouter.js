//import thư viện express
const orderRouter = require('express').Router();

//import Controller
const OrderController = require('../controllers/orderController');



orderRouter.get('/orders', OrderController.getAllOrders);

//create new order
orderRouter.post('/users/:userId/orders', OrderController.createNewOrder);

orderRouter.get('/:orderId', OrderController.getOrderById);

orderRouter.put('/orders/:orderId', OrderController.updateOrderById);

orderRouter.delete('/users/:userId/orders/:orderId', OrderController.deleteOrderById);

orderRouter.delete('/orders', OrderController.deleteOrderByOrderCode);


//create order by random orderCode
orderRouter.post('/orders', OrderController.createNewOrderByRandomOrderCode)

//export 
module.exports = orderRouter